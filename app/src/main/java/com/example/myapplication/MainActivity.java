package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * @author Ernesto "Mefus" Muñoz Nieves -
 * <a href="mailto:mefus32@gmail.com" >mefus32@gmail.com</a>
 */

/**
 * @author Ernesto "ernt" Muñoz Nieves -
 * <a href="mailto:ernestn@ciencias.unam.mx" >ernestn@ciencias.unam.mx</a>
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}